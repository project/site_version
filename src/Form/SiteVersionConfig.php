<?php

namespace Drupal\site_version\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_version\SiteVersionHelper;

/**
 * Site Version Configuration form.
 */
class SiteVersionConfig extends ConfigFormBase {

  /**
   * Get Form ID.
   */
  public function getFormId() {
    return 'site_version_config_form';
  }

  /**
   * Build Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = SiteVersionHelper::getConfig();

    $form['version'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Version"),
      '#default_value' => $config->get('version'),
    ];
    $form['build'] = [
      '#type' => 'number',
      '#title' => $this->t("Build"),
      '#default_value' => $config->get('build'),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Description"),
      '#default_value' => $config->get('description'),
      '#description' => $this->t("Description of the version or site"),
    ];

    // API.
    $form['json'] = [
      '#type' => 'details',
      '#title' => $this->t("JSON API"),
      '#open' => $config->get('json_enabled') ? TRUE : FALSE,
    ];
    $form['json']['json_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Enable site version JSON API"),
      '#default_value' => $config->get('json_enabled'),
    ];
    $form['json']['json_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Access API KEY"),
      '#default_value' => $config->get('json_api_key'),
    ];
    if (!empty($config->get('json_api_key'))) {
      $link = SiteVersionHelper::getLink();
      $form['json']['json_api_key_display'] = [
        '#type' => 'markup',
        '#markup' => "<p><a href='$link'>$link</a></p>",
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('json_enabled') && empty($form_state->getValue('json_api_key'))) {
      $form_state->setErrorByName('json_api_key', $this->t('API Key must not empty.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get config.
    $config = SiteVersionHelper::getConfig(TRUE);

    // Update.
    $fields = [
      'version',
      'build',
      'description',
      'json_enabled',
      'json_api_key',
    ];
    foreach ($fields as $field) {
      $config->set($field, $form_state->getValue($field));
    }
    $config->set('changed', time());

    // Save config.
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      SiteVersionHelper::getConfigName(),
    ];
  }

}
