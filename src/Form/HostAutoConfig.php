<?php

namespace Drupal\site_version\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_version\SiteVersionHelper;

/**
 * Site Version Configuration form.
 */
class HostAutoConfig extends FormBase {

  /**
   * Get Form ID.
   */
  public function getFormId() {
    return 'site_version_host_autoconfig_form';
  }

  /**
   * Build Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = SiteVersionHelper::getConfig();

    $form['url'] = [
      '#type' => 'url',
      '#required' => TRUE,
      '#title' => $this->t("Host Url"),
      '#description' => $this->t("Full URL including Arguments"),
      '#default_value' => $config->get('host_url'),
    ];

    if (!$config->get('json_enabled')) {
      $form['info'] = [
        '#markup' => 'You are not enabled the JSON API, This will automatically enable.',
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      ];
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#default_value' => 'Start',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $hostUrl = $form_state->getValue('url');

    // Save config.
    $config = SiteVersionHelper::getConfig(TRUE);
    if ($config->get('host_url') != $hostUrl) {
      $config->set('host_url', $hostUrl);
      $config->save();
    }
    if (!$config->get('json_enabled')) {
      $this->messenger()->addMessage("JSON API is enabled");
      $config->set('json_enabled', TRUE);
      $config->save();
    }

    $localLink = SiteVersionHelper::getLink();

    $link = $hostUrl . "&url=" . base64_encode($localLink);
    $response = file_get_contents($link);

    $resp = json_decode($response, TRUE);
    if (isset($resp['error'])) {
      $this->messenger()->addError($resp['error']);
    }
    else {
      $this->messenger()->addMessage("Success : " . $resp['message']);
    }

    $form_state->setRebuild(TRUE);

  }

}
