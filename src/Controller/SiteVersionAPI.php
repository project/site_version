<?php

namespace Drupal\site_version\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\site_version\SiteVersionHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Simple Version API.
 */
class SiteVersionAPI extends ControllerBase {

  /**
   * Request Stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Constructor.
   *
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Get live visitors deta.
   */
  public function main() {

    $output = [];
    $output["api_version"] = 1;
    $output["time"] = time();

    $api_key = $this->requestStack->getCurrentRequest()->query->get('api_key');

    $config = SiteVersionHelper::getConfig(FALSE);

    if (empty($config->get('json_enabled'))) {
      $output["error"] = "Disabled";
    }
    elseif (empty($api_key)) {
      $output["error"] = "API Key is empty";
    }
    elseif ($config->get('json_api_key') !== $api_key) {
      $output["error"] = "API Key error";
    }
    else {
      $output['data'] = SiteVersionHelper::getVersionDetails();
    }
    return new JsonResponse($output);
  }

}
