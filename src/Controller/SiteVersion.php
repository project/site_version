<?php

namespace Drupal\site_version\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\site_version\SiteVersionHelper;

/**
 * Site version main controller.
 */
class SiteVersion extends ControllerBase {

  /**
   * Show main information.
   */
  public function main() {
    $output = [];

    $datas = [];
    $lables = ['Name', 'Value'];

    // Get version info.
    $result = SiteVersionHelper::getVersionDetails();

    foreach ($result as $key => $value) {
      $datas[] = [$key, $value];
    }

    $output[] = [
      '#theme' => 'table',
      '#rows' => $datas,
      '#header' => $lables,
      '#empty' => '',
    ];

    return $output;
  }

}
