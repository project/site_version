<?php

namespace Drupal\site_version;

/**
 * Site Version Helper functions.
 */
class SiteVersionHelper {

  /**
   * Get Configuration Name.
   */
  public static function getConfigName() {
    return 'site_version.settings';
  }

  /**
   * Get Configuration Object.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   The configuration object.
   */
  public static function getConfig($editable = FALSE) {
    if ($editable) {
      $config = \Drupal::configFactory()->getEditable(static::getConfigName());
    }
    else {
      $config = \Drupal::config(static::getConfigName());
    }
    return $config;
  }

  /**
   * Get Version details.
   *
   * @return array
   *   Version details
   */
  public static function getVersionDetails() {
    // Get config.
    $config = SiteVersionHelper::getConfig(FALSE);

    $output = [];
    // Update.
    $fields = [
      'version',
      'build',
      'description',
    ];
    foreach ($fields as $field) {
      $output[$field] = $config->get($field);
    }
    $output['changed'] = date(DATE_ISO8601, $config->get("changed"));

    // Get system info.
    $config_sys = \Drupal::config("system.site");
    $output['name'] = $config_sys->get("name");
    $output['site_uuid'] = $config_sys->get("uuid");
    $output['core'] = \Drupal::VERSION;

    return $output;
  }

  /**
   * Generate a random key.
   */
  public static function getLink() {
    $config = SiteVersionHelper::getConfig();
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $json_api_key = $config->get('json_api_key');
    return "$host/site-version/json?api_key=$json_api_key";
  }

  /**
   * Generate a random key.
   */
  public static function generateKey() {
    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    return substr(str_shuffle($str_result), 0, 32);
  }

}
