<?php

namespace Drupal\Tests\site_version\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test uninstall functionality of Site Version module.
 *
 * @group site_version
 */
class SiteVersionBasicTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['site_version'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that we can uninstall the module without mishap.
   */
  public function testUninstallModule() {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $installer */
    $installer = $this->container->get('module_installer');
    $this->assertTrue($installer->uninstall(['site_version']));
  }

}
